<?php
namespace Mock\MockDataGenerator;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Template\Model\WbjTemplate;
use Base\Template\Repository\WbjTemplateRepository;

use Base\ResourceCatalogData\Model\WbjItemsData;
use Base\ResourceCatalogData\Model\WbjSearchData;
use Base\ResourceCatalogData\Strategy\IDataStrategy;
use Base\ResourceCatalogData\Strategy\DataStrategyFactory;

use Mock\MockDataGenerator\SearchDataTrait;
use Mock\MockDataGenerator\IMockDataGenerator;

class WbjSearchDataGenerator implements IMockDataGenerator
{
    use PrintLogTrait, SearchDataTrait;
    
    protected function getWbjTemplateRepository() : WbjTemplateRepository
    {
        return new WbjTemplateRepository();
    }

    public function fetchWbjTemplate(int $id)
    {
        return $this->getWbjTemplateRepository()->fetchOne($id);
    }

    protected function getDataStrategyFactory() : DataStrategyFactory
    {
        return new DataStrategyFactory();
    }

    protected function fetchDataStrategy(int $type) : IDataStrategy
    {
        return $this->getDataStrategyFactory()->getStrategy($type);
    }

    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function mock($parameter, $number = 10)
    {
        if (!is_numeric($parameter) || !is_numeric($number)) {
            $this->printLog("委办局资源目录数据:参数格式不正确,请重新输入".PHP_EOL);
            return false;
        }

        $wbjTemplate = $this->fetchWbjTemplate($parameter);

        if ($wbjTemplate instanceof INull) {
            $this->printLog("资源目录不存在,请重新输输入".PHP_EOL);
            return false;
        }
        
        $items = $wbjTemplate->getItems();
        
        $ids = array();
        $failNumber = 0;

        $this->printLog("委办局资源目录数据,mock开始时间为 ".date('Y-m-d H:i:s').PHP_EOL);

        for ($i = 0; $i < $number; $i++) {
            $data = array();

            foreach ($items as $key => $item) {
                unset($key);
                $item['subjectCategory'] = $wbjTemplate->getSubjectCategory();
                $data[$item['identify']] = $this->fetchDataStrategy($item['type'])->mock($item);
            }

            $wbjSearchData = new WbjSearchData();
            $wbjSearchData->setTemplate($wbjTemplate);

            $wbjItemsData = new WbjItemsData();
            $wbjItemsData->setData($data);
            $wbjSearchData->setItemsData($wbjItemsData);
            $wbjSearchData->setSourceUnit($wbjTemplate->getSourceUnit());
            $wbjSearchData = $this->fetchSearchData($wbjSearchData);

            if ($wbjSearchData->add()) {
                $ids[] = $wbjSearchData->getId();
                $this->printLog("委办局资源目录数据,新增成功,成功id为 ".$wbjSearchData->getId().PHP_EOL);
            } else {
                $failNumber++;
                $this->printLog("委办局资源目录数据,新增失败,失败id为 ".$wbjSearchData->getId().PHP_EOL);
            }
        }

        $this->printLog("委办局资源目录数据,新增完成,结束时间为 ".date('Y-m-d H:i:s')."新增成功的数据总数为 ".count($ids).
        ", 新增失败的数据总量为 ".$failNumber.", 实际需要新增的数据总数为 ".$number.PHP_EOL);

        return true;
    }
}
