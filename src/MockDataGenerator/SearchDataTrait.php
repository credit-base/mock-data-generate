<?php
namespace Mock\MockDataGenerator;

use Base\ResourceCatalogData\Model\SearchData;
use Base\ResourceCatalogData\Model\ISearchDataAble;

use Base\Crew\Model\Crew;

trait SearchDataTrait
{
    protected function fetchSearchData(SearchData $searchData) : SearchData
    {
        $data = $searchData->getItemsData()->getData();

        $expirationDate = isset($data['YXQX']) ? $data['YXQX'] : SearchData::EXPIRATION_DATE_DEFAULT;

        $hash = md5(serialize($data));

        $subjectCategory = current($searchData->getTemplate()->getSubjectCategory());

        $searchData->setCrew(new Crew(1));
        $searchData->setSubjectCategory($subjectCategory);
        $searchData->setDimension($searchData->getTemplate()->getDimension());
        $searchData->setExpirationDate($expirationDate);
        $searchData->setHash($hash);

        return $searchData;
    }
}
