<?php
namespace Mock\MockDataGenerator;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Template\Model\GbTemplate;
use Base\Template\Repository\GbTemplateRepository;

use Base\ResourceCatalogData\Model\GbItemsData;
use Base\ResourceCatalogData\Model\GbSearchData;
use Base\ResourceCatalogData\Strategy\IDataStrategy;
use Base\ResourceCatalogData\Strategy\DataStrategyFactory;

use Base\UserGroup\Model\UserGroup;

use Mock\MockDataGenerator\SearchDataTrait;
use Mock\MockDataGenerator\IMockDataGenerator;

class GbSearchDataGenerator implements IMockDataGenerator
{
    use PrintLogTrait, SearchDataTrait;
    
    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return new GbTemplateRepository();
    }

    public function fetchGbTemplate(int $id)
    {
        return $this->getGbTemplateRepository()->fetchOne($id);
    }

    protected function getDataStrategyFactory() : DataStrategyFactory
    {
        return new DataStrategyFactory();
    }

    protected function fetchDataStrategy(int $type) : IDataStrategy
    {
        return $this->getDataStrategyFactory()->getStrategy($type);
    }

    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function mock($parameter, $number = 10)
    {
        if (!is_numeric($parameter) || !is_numeric($number)) {
            $this->printLog("国标资源目录数据:参数格式不正确,请重新输入".PHP_EOL);
            return false;
        }

        $gbTemplate = $this->fetchGbTemplate($parameter);

        if ($gbTemplate instanceof INull) {
            $this->printLog("资源目录不存在,请重新输输入".PHP_EOL);
            return false;
        }
        
        $items = $gbTemplate->getItems();
        
        $ids = array();
        $failNumber = 0;

        $this->printLog("国标资源目录数据,mock开始时间为 ".date('Y-m-d H:i:s').PHP_EOL);

        for ($i = 0; $i < $number; $i++) {
            $data = array();

            foreach ($items as $key => $item) {
                unset($key);
                $item['subjectCategory'] = $gbTemplate->getSubjectCategory();
                $data[$item['identify']] = $this->fetchDataStrategy($item['type'])->mock($item);
            }

            $gbSearchData = new GbSearchData();
            $gbSearchData->setTemplate($gbTemplate);

            $gbItemsData = new GbItemsData();
            $gbItemsData->setData($data);
            $gbSearchData->setItemsData($gbItemsData);
            $gbSearchData->setSourceUnit(new UserGroup(1));
            $gbSearchData = $this->fetchSearchData($gbSearchData);

            if ($gbSearchData->add()) {
                $ids[] = $gbSearchData->getId();
                $this->printLog("国标资源目录数据,新增成功,成功id为 ".$gbSearchData->getId().PHP_EOL);
            } else {
                $failNumber++;
                $this->printLog("国标资源目录数据,新增失败,失败id为 ".$gbSearchData->getId().PHP_EOL);
            }
        }

        $this->printLog("国标资源目录数据,新增完成,结束时间为 ".date('Y-m-d H:i:s')."新增成功的数据总数为 ".count($ids).
        ", 新增失败的数据总量为 ".$failNumber.", 实际需要新增的数据总数为 ".$number.PHP_EOL);

        return true;
    }
}
