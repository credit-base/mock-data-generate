<?php
namespace Mock\MockDataGenerator;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Template\Model\BjTemplate;
use Base\Template\Repository\BjTemplateRepository;

use Base\ResourceCatalogData\Model\BjItemsData;
use Base\ResourceCatalogData\Model\BjSearchData;
use Base\ResourceCatalogData\Strategy\IDataStrategy;
use Base\ResourceCatalogData\Strategy\DataStrategyFactory;

use Mock\MockDataGenerator\SearchDataTrait;
use Mock\MockDataGenerator\IMockDataGenerator;

class BjSearchDataGenerator implements IMockDataGenerator
{
    use PrintLogTrait, SearchDataTrait;
    
    protected function getBjTemplateRepository() : BjTemplateRepository
    {
        return new BjTemplateRepository();
    }

    public function fetchBjTemplate(int $id)
    {
        return $this->getBjTemplateRepository()->fetchOne($id);
    }

    protected function getDataStrategyFactory() : DataStrategyFactory
    {
        return new DataStrategyFactory();
    }

    protected function fetchDataStrategy(int $type) : IDataStrategy
    {
        return $this->getDataStrategyFactory()->getStrategy($type);
    }

    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function mock($parameter, $number = 10)
    {
        if (!is_numeric($parameter) || !is_numeric($number)) {
            $this->printLog("本级资源目录数据:参数格式不正确,请重新输入".PHP_EOL);
            return false;
        }

        $bjTemplate = $this->fetchBjTemplate($parameter);

        if ($bjTemplate instanceof INull) {
            $this->printLog("资源目录不存在,请重新输输入".PHP_EOL);
            return false;
        }
        
        $items = $bjTemplate->getItems();
        
        $ids = array();
        $failNumber = 0;

        $this->printLog("本级资源目录数据,mock开始时间为 ".date('Y-m-d H:i:s').PHP_EOL);

        for ($i = 0; $i < $number; $i++) {
            $data = array();

            foreach ($items as $key => $item) {
                unset($key);
                $item['subjectCategory'] = $bjTemplate->getSubjectCategory();
                $data[$item['identify']] = $this->fetchDataStrategy($item['type'])->mock($item);
            }

            $bjSearchData = new BjSearchData();
            $bjSearchData->setTemplate($bjTemplate);

            $bjItemsData = new BjItemsData();
            $bjItemsData->setData($data);
            $bjSearchData->setItemsData($bjItemsData);
            $bjSearchData->setSourceUnit($bjTemplate->getSourceUnit());
            $bjSearchData = $this->fetchSearchData($bjSearchData);

            if ($bjSearchData->add()) {
                $ids[] = $bjSearchData->getId();
                $this->printLog("本级资源目录数据,新增成功,成功id为 ".$bjSearchData->getId().PHP_EOL);
            } else {
                $failNumber++;
                $this->printLog("本级资源目录数据,新增失败,失败id为 ".$bjSearchData->getId().PHP_EOL);
            }
        }

        $this->printLog("本级资源目录数据,新增完成,结束时间为 ".date('Y-m-d H:i:s')."新增成功的数据总数为 ".count($ids).
        ", 新增失败的数据总量为 ".$failNumber.", 实际需要新增的数据总数为 ".$number.PHP_EOL);

        return true;
    }
}
