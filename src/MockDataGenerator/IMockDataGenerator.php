<?php
namespace Mock\MockDataGenerator;

interface IMockDataGenerator
{
    public function mock($parameter, $number = 10);
}
